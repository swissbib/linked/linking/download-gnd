#     download-gnd
#     Copyright (C) 2019 Project swissbib <http://swissbib.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published
#     by the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gzip
import shutil
import requests
import logging
import json
import sys
import os
from datetime import date
from simple_elastic import ElasticIndex


def download_file(path: str, base_file_name: str, today: str):
    sink = os.path.join(path, base_file_name + '-' + today + '.gz')
    logging.info(f'Begin download of lobid data into {sink}.')
    with requests.get(f'http://lobid.org/gnd/search?format=jsonl',
                      headers=headers,
                      stream=True) as response:
        response.raise_for_status()
        logging.debug(response.status_code)
        with gzip.open(sink, 'wb') as download:
            for chunk in response.iter_content(chunk_size=8192):
                logging.debug(chunk)
                if chunk:
                    download.write(chunk)
    logging.info(f'Finished download of lobid data into {sink}.')
    # After download finishes move the file to folder where it will be read!
    shutil.move(sink, os.path.join('/data', f'{base_file_name}-{today}.gz'))


def create_indices():
    create_index(app_configs['DifferentiatedPerson'])
    create_index(app_configs['CorporateBody'])
    create_index(app_configs['ConferenceOrEvent'])
    create_index(app_configs['UndifferentiatedPerson'])
    create_index(app_configs['Work'])
    create_index(app_configs['PlaceOrGeographicName'])
    create_index(app_configs['SubjectHeading'])


def create_index(index_configs: dict):
    with open(f'/configs/mapping.{index_configs["mapping"]}.json', 'r') as fp:
        index_mapping = json.load(fp)

    index = ElasticIndex(f'{index_configs["name"]}-{today_as_str}',
                         url=elastic_search_host,
                         mapping=index_mapping['mappings'],
                         settings=index_mapping['settings'])

    if "alias" in index_configs:
        index.add_to_alias(f'{index_configs["alias"]}')


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout,
                        level=logging.INFO,
                        format='[%(levelname)s]:%(module)s:%(message)s')
    logging.info("GND Downloader Version 1.2.0")

    headers = {
        'Accept-Encoding': 'gzip',
        'User-Agent': 'Bulk Download Script v1.2.0 (https://gitlab.com/swissbib/linked/linking/download-gnd)',
        'From': 'jonas.waeber@unibas.ch'
    }
    today_as_str = date.today().isoformat()
    data_path = '/data'
    tmp_path = '/tmp'

    with open(f'/configs/app.json', 'r') as configs:
        app_configs = json.load(configs)
    elastic_search_host = app_configs["host"]
    logging.info('Removing old files.')
    for file in os.listdir(data_path):
        os.remove(os.path.join(data_path, file))
    # creates a new index to index the downloads to.
    create_indices()
    logging.info('Dump will be saved to: ' + data_path)
    download_file(tmp_path, 'gnd-dump', today_as_str)
